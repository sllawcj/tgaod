package TGAOD.screen;

import TGAOD.BaseGame;

import com.badlogic.gdx.Screen;

public class MainScreen implements Screen
{
	public static BaseGame baseGame;

	public MainScreen(BaseGame baseGame)
	{
		this.baseGame = baseGame;
	}

	@Override
	public void render(float delta)
	{
		baseGame.setScreen(new GamePlay());
	}

	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generatwed method stub

	}

	@Override
	public void show()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{

	}
}
