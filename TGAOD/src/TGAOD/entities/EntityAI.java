package TGAOD.entities;

import TGAOD.Util;
import TGAOD.controller.GameEngine;
import TGAOD.pathfinding.GridLocation;
import TGAOD.pathfinding.GridPath;
import TGAOD.pathfinding.GridPathfinding;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class EntityAI extends Entity
{

	public GridPathfinding p = new GridPathfinding();
	public GridPath path = p.getPath(new GridLocation(
			(int) (body.getPosition().x + 0.5f),
			(int) (body.getPosition().y + 0.5f), false), new GridLocation(
			(int) (5 + 0.5f), (int) (5 + 0.5f), true), engine.map.paths);
	ShapeRenderer pathRenderer = new ShapeRenderer();
	float SpinSpeed;
	float SpinAccel;

	Vector2 goalPosition;

	boolean avoiding;

	public EntityAI(Vector2 position, GameEngine engine)
	{
		super(100, 10, 10, position, engine);
		SpinSpeed = (float) Math.PI * 2;
		SpinAccel = SpinSpeed * 4;
		goalPosition = body.getPosition();
	}

	public void update(float delta)
	{
		if ((Math
				.abs(engine.player.body.getPosition().x - body.getPosition().x) < 20)
				&& (Math.abs(engine.player.body.getPosition().y
						- body.getPosition().y) < 20))
		{
			avoiding = false;
            seek(engine.player.body.getPosition());
            wiskers();
		}
	}

	protected float getCollisionRadius()
	{
		return 30;
	}

	public void onDeath()
	{

	}

	public void seek(Vector2 goal)
	{
		goalPosition = new Vector2(goal);

		if ((Math.abs(goal.x - body.getPosition().x) < 15)
				&& (Math.abs(goal.y - body.getPosition().y) < 15))
		{
			Vector2 goaldir = new Vector2();
			goaldir.set(goalPosition);
			goaldir.sub(body.getPosition());
			Vector2 currentdir = getDirection();
			float delta_angle = Util.signedAngle(currentdir, goaldir);

			delta_angle *= SpinSpeed;

			float angle_accel = delta_angle - body.getAngularVelocity();
			angle_accel = Util.scaleback(angle_accel, SpinAccel);

			body.applyAngularImpulse(angle_accel, true);

			walkTowards(goalPosition);

			// Footing.
			Vector2 projVel = currentdir.scl(body.getLinearVelocity().dot(
					currentdir));
			Vector2 footingvel = projVel.sub(body.getLinearVelocity());

			// body.applyLinearImpulse(footingvel, body.getWorldCenter(), true);
		} else
		{
			walkTowards(body.getPosition());
		}
	}

	private void avoid(Vector2 wiskervec)
	{
		float wisklen = wiskervec.len();
		WiskerRay wisker = new WiskerRay(engine.player);
		engine.physics.rayCast(wisker, body.getPosition(),
				new Vector2(body.getPosition()).add(wiskervec));

		float dist = new Vector2(wisker.point).sub(body.getPosition()).len();

		if (!wisker.hit)
		{
			return;
		}
		float strengthmod = (1 - (dist - 1) / (wisklen - 1));
		float strength = strengthmod * 120;
		body.applyForce(wiskervec.scl(-1f * strength), body.getWorldCenter(),
				true);

	}

	private void wiskers()
	{
		int wiskers = 8;
		Vector2 wisk = new Vector2(0, 1);
		for (int i = 0; i < wiskers; ++i)
		{
			wisk.set(0, 1);
			wisk.setAngle(360.0f / wiskers * i);
			wisk.rotate(body.getAngle() * 180 / (float) Math.PI).scl(2);
			avoid(wisk);
		}
	}

	public void pathfinding(Vector2 goalPos)
	{
		p = new GridPathfinding();
		GridLocation here = new GridLocation((int) (body.getPosition().x) / 2,
				(int) (body.getPosition().y) / 2, false);
		GridLocation there = new GridLocation((int) (goalPos.x) / 2,
				(int) (goalPos.y) / 2, true);
		path = p.getPath(here, there, engine.map.paths);

		int x = path.getList().get(path.getList().size() - 2).getX();
		int z = path.getList().get(path.getList().size() - 2).getY();

		facePosition(new Vector2(x * 2 + 1, z * 2 + 1));

		walkTowards(new Vector2(x * 2 + 1, z * 2 + 1));
	}

	public void drawPath()
	{
		pathRenderer.setProjectionMatrix(engine.camera.combined);
		pathRenderer.begin(ShapeType.Line);

		if (path != null)
		{
			for (int i = 1; i < path.getList().size(); ++i)
			{
				pathRenderer.line(path.getList().get(i - 1).getX() * 2 + 1,
						path.getList().get(i - 1).getY() * 2 + 1, path
								.getList().get(i).getX() * 2 + 1, path
								.getList().get(i).getY() * 2 + 1);
			}
		}

		int wiskers = 8;
		Vector2 wisk = new Vector2(0, 1);
		for (int i = 0; i < wiskers; ++i)
		{
			wisk.set(0, 1);
			wisk.setAngle(360.0f / wiskers * i);
			wisk.rotate(body.getAngle() * 180 / (float) Math.PI).scl(2)
					.add(body.getPosition());
			pathRenderer.line(body.getPosition().x, body.getPosition().y,
					wisk.x, wisk.y);
		}

		pathRenderer.line(body.getPosition().x, body.getPosition().y,
				goalPosition.x, goalPosition.y);

		pathRenderer.end();
	}

}
