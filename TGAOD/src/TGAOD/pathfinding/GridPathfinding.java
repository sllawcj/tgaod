package TGAOD.pathfinding;

public class GridPathfinding
{

	GridAstar astar;
	GridHeuristicManathan heuristic;

	public GridPathfinding()
	{
		heuristic = new GridHeuristicManathan();
	}

	public GridPath getPath(GridLocation s, GridLocation e, GridMap m)
	{
		GridLocation start = (GridLocation) s;
		GridLocation end = (GridLocation) e;
		GridMap map = (GridMap) m;

		astar = new GridAstar(start, end, map, heuristic);

		return astar.getPath();
	}

}
