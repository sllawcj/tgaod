package TGAOD.entities;

import TGAOD.BodyPositionable;
import TGAOD.Updatable;
import TGAOD.Util;
import TGAOD.controller.GameEngine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity extends BodyPositionable implements Updatable
{
	public static byte UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3;

	public int health, maxHealth;

	public GameEngine engine;
	public float speed;
	public float accel;
	public Texture walkSheet;
	public static TextureRegion[] walkFrames;
	private static final int FRAME_COLS = 10;
	private static final int FRAME_ROWS = 1;

	public Entity(int maxHealth, float speed, float accel, Vector2 position,
			GameEngine engine)
	{
		super(null);
		this.body = Util.createCircleBody(position.x, position.y,
				getCollisionRadius(), engine);
		this.health = this.maxHealth = maxHealth;
		this.engine = engine;
		this.engine.entities.add(this);
		this.speed = speed;
		this.accel = accel;
		
		walkSheet = new Texture(
				Gdx.files.internal("assets/characters-32x32.png"));
		TextureRegion[][] tmp = TextureRegion.split(walkSheet,
				walkSheet.getWidth() / FRAME_COLS, walkSheet.getHeight()
						/ FRAME_ROWS); // #10
		walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++)
		{
			for (int j = 0; j < FRAME_COLS; j++)
			{
				walkFrames[index++] = tmp[i][j];
			}
		}
	}

	public void kill()
	{
		health = 0;
		engine.physics.destroyBody(body);
		onDeath();
	}

	public Vector2 getDirection()
	{
		return new Vector2((float) Math.cos(body.getTransform().getRotation()),
				(float) Math.sin(body.getTransform().getRotation()));
	}

	public void walk(Vector2 direction)
	{
		direction = new Vector2(direction);
		direction.nor();
		Vector2 dirvel = direction.scl(speed);
		Vector2 dvel = dirvel.sub(body.getLinearVelocity());
		if (dvel.len2() > accel * accel)
		{
			dvel.scl(speed / (float) dvel.len());
		}

		body.applyLinearImpulse(dvel, body.getWorldCenter(), true);
	}

	public void walkTowards(Vector2 position)
	{
		walk(new Vector2(position).sub(body.getPosition()));
	}

	public void faceDirection(Vector2 dir)
	{
		float angle = dir.angle() * ((float) Math.PI / 180);
		body.getTransform().setRotation(angle);
		body.setAngularVelocity(0);
	}

	public void facePosition(Vector2 pos)
	{
		pos = new Vector2(pos);
		Vector2 dir = (pos.sub(body.getPosition()));
		faceDirection(dir);
	}

	protected abstract float getCollisionRadius();

	public abstract void onDeath();

}