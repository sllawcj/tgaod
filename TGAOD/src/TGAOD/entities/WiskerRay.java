package TGAOD.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;

public class WiskerRay implements RayCastCallback
{
	Player player;
	public Vector2 point;
	public Vector2 normal;
	public float dist;
	public boolean hit;

	WiskerRay(Player player)
	{
		this.player = player;
		point = new Vector2();
		hit = false;
	}

	@Override
	public float reportRayFixture(Fixture fixture, Vector2 point,
			Vector2 normal, float fraction)
	{

		if (player.body.getFixtureList().contains(fixture))
		{
			return -1;
		}

		hit = true;
		this.point = new Vector2(point);
		this.normal = new Vector2(normal);
		dist = fraction;
		return fraction;
	}

}
