package TGAOD;

public interface Updatable
{
	public abstract void update(float delta);
}