package TGAOD.controller;

import java.util.ArrayList;
import java.util.HashMap;

import TGAOD.entities.Entity;
import TGAOD.entities.EntityAI;
import TGAOD.entities.Player;
import TGAOD.world.Map;
import TGAOD.pathfinding.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class GameEngine
{
	public Map map;
	public Player player;
	public ArrayList<Entity> entities;
	public ArrayList<Entity> removeEntities;
	public World physics;
	public OrthographicCamera camera;
	public SpriteBatch batch;
	final int ENEMY = 10;

	Box2DDebugRenderer debugRenderer;
	/** pixel perfect projection for font rendering */
	Matrix4 normalProjection = new Matrix4();
	public static int RAYS_PER_BALL = 128;
	private float physicsTimeLeft;
	private final static int MAX_FPS = 30;
	private final static int MIN_FPS = 15;
	public final static float TIME_STEP = 1f / MAX_FPS;
	private final static float MAX_STEPS = 1f + MAX_FPS / MIN_FPS;
	private final static float MAX_TIME_PER_FRAME = TIME_STEP * MAX_STEPS;
	private final static int VELOCITY_ITERS = 10000;
	private final static int POSITION_ITERS = 10000;
	float timespent = 0f;
	public boolean gameWin = false;
	public boolean gameLose = false;
	public boolean running = true;

	void CreateCamera()
	{
		camera = new OrthographicCamera(48, 32);
		camera.position.set(0, 16, 0);
		camera.update();
	}

	void CreateBatch()
	{
		batch = new SpriteBatch();
	}

	void CreatePhysics()
	{
		physics = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
	}

	public GameEngine()
	{
		reset();
	}

	Vector2 lastCulledPlayerPosition;
	HashMap<Vector2, Body> shownBlocksList;

	int roomCount = 50;

	class bodyUserData
	{
		boolean markedForDelete = false;
	}

	void PhysicsStep(float dt)
	{
		physicsTimeLeft += dt;
		if (physicsTimeLeft > MAX_TIME_PER_FRAME)
			physicsTimeLeft = MAX_TIME_PER_FRAME;

		boolean stepped = false;
		while (physicsTimeLeft >= TIME_STEP)
		{
			physics.step(TIME_STEP, VELOCITY_ITERS, POSITION_ITERS);
			physicsTimeLeft -= TIME_STEP;
			stepped = true;
		}

		if (stepped)
		{

			camera.position.set(new Vector3(player.body.getPosition().x,
					player.body.getPosition().y, 0));
		}

	}

	public void update(float dt)
	{
		for (int i = entities.size() - 1; i >= 0; --i)
			{
				if ((Math.abs(this.player.body.getPosition().x - entities.get(i).body.getPosition().x) < 20)
				&& (Math.abs(this.player.body.getPosition().y
						- entities.get(i).body.getPosition().y) < 20))
					
				{
					entities.get(i).update(dt);
				}
			}

		PhysicsStep(dt);
		entities.removeAll(removeEntities);
		removeEntities.clear();

	}

	public void reset()
	{
		entities = new ArrayList<Entity>();
		removeEntities = new ArrayList<Entity>();
		CreateCamera();
		CreatePhysics();
		CreateBatch();
		map = new Map(this);
		int[] startend = map.generate();
		Vector2 properStart = new Vector2(2, 2);// new Vector2(startend[0] * 2,
												// startend[1] * 2);
		player = new Player(properStart, this);

		FixtureDef fixtureDef = new FixtureDef();
		CircleShape shape = new CircleShape();

		lastCulledPlayerPosition = new Vector2(player.body.getPosition().x,
				player.body.getPosition().y);
		shownBlocksList = new HashMap<Vector2, Body>();
		Gdx.gl.glClearColor(0, 0, 0, 1);// black

		normalProjection.setToOrtho2D(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		timespent = 0f;
		gameWin = false;
		gameLose = false;
		generateEnemies();
	}

	private void generateEnemies()
	{
		for (int i = 0; i < ENEMY; i++)
		{
			entities.add(new EntityAI(map.getEmptySpace(), this));
		}
	}

	public void render(float dt)
	{
		if (!running)
			return;
		camera.update();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.disableBlending();
		batch.begin();
		batch.enableBlending();

		map.Draw();
		batch.draw(player.playerImage, player.body.getPosition().x - 1,
				player.body.getPosition().y - 1, 1, 1, 2, 2, 1, 1, 0);

		batch.end();

		for (Entity e : entities)
		{
			if (e instanceof EntityAI)
			{
				((EntityAI) e).drawPath();
			}
		}
	}
}
