package TGAOD.entities;

import TGAOD.controller.GameEngine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Player extends Entity
{
	public TextureRegion playerImage;

	public Player(Vector2 pos, GameEngine engine)
	{
		super(100, 20, 20, pos, engine);
		playerImage = Entity.walkFrames[0];
	}

	public boolean stopPlayerUpdatingCharacter = false;

	public void update(float delta)
	{
		body.setTransform(
				body.getTransform().getPosition(),
				(float) (Math.atan2(Gdx.input.getX() - Gdx.graphics.getWidth()
						/ 2, Gdx.input.getY() - Gdx.graphics.getHeight() / 2) - Math.PI / 2));

		Vector2 dir = new Vector2(0, 0);
		if (!stopPlayerUpdatingCharacter)
		{
			if (Gdx.input.isKeyPressed(Input.Keys.W))
			{
				dir.y += 1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.A))
			{
				dir.x += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.S))
			{
				dir.y += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.D))
			{
				dir.x += 1;
			}

		}

		if (dir.len() != 0)
			dir.scl(1.0f / dir.len());

		walk(dir);
	}

	protected float getCollisionRadius()
	{
		return 26;
	}

	@Override
	public void onDeath()
	{
		// TODO Auto-generated method stub

	}

}